import React from 'react';
import {
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  Badge,
} from '@chakra-ui/react';
import { MessageProps } from '.';

type MamaTableProps = {
  data: MessageProps[]
}

export function formatDate(date: string) {
  return new Date(date).toLocaleString('id-ID');
}

const StatusBadge = ({ status }: { status: string }) => (
  <Badge colorScheme={status === 'waiting' ? 'yellow' : status === 'failed' ? 'red' : 'green'}>
    { status}
  </Badge>
)

export default function MamaTable({ data }: MamaTableProps) {
  return (
    <Table variant="simple">
      <Thead>
        <Tr>
          <Th>Date</Th>
          <Th>Phone Number</Th>
          <Th>Message</Th>
          <Th>Status</Th>
        </Tr>
      </Thead>
      <Tbody>
        {data?.map((msg: MessageProps) => (
          <Tr key={msg.id}>
            <Td>{formatDate(msg.createdAt as string)}</Td>
            <Td>{msg.phoneNumber}</Td>
            <Td>{msg.message}</Td>
            <Td>
              <StatusBadge status={msg.status as string} />
            </Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  )
}
