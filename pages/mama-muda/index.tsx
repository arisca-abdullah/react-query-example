import React, { useState } from "react";
import { useQuery, useQueryClient, useMutation } from 'react-query';
import { useForm } from 'react-hook-form';
import {
  Box,
  Flex,
  Text,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Button,
  Textarea,
} from "@chakra-ui/react";
import Layout from "../../components/Layout";
import MamaTable from "./MamaTable";

export type MessageProps = {
  id?: number,
  createdAt?: string,
  phoneNumber: string,
  message: string,
  status?: string
}

const getMessages = async () => {
  const URL = 'http://localhost:3000/api/message';
  const result = await fetch(URL);
  return await result.json();
}

const submitMessage = async (data: MessageProps) => {
  const URL = 'http://localhost:3000/api/message';
  const response = await fetch(URL, {
    method: 'POST',
    body: JSON.stringify(data)
  });

  if (!response.ok) {
    throw new Error("An error has occured");
  }

  return await response.json();
}

export default function MamaMuda() {
  const [errMessage, setErrMessage] = useState('');
  const queryClient = useQueryClient();
  const { handleSubmit, errors, register, reset, clearErrors } = useForm<MessageProps>();
  const { data, isSuccess } = useQuery('Messages', getMessages, {
    staleTime: 15000,
    refetchInterval: 15000
  });
  const mutation = useMutation(submitMessage, {
    onMutate: async (newMessage) => {
      await queryClient.cancelQueries("Messages");
      const previousMessages = queryClient.getQueryData<MessageProps[]>("Messages");

      if (previousMessages) {
        newMessage = {
          ...newMessage,
          id: previousMessages.length + 1,
          createdAt: new Date().toISOString(),
          status: 'waiting'
        }
        const finalMessages = [...previousMessages, newMessage];
        queryClient.setQueryData('Messages', finalMessages);
      }

      return { previousMessages };
    },
    onSettled: async (data, error: any) => {
      console.log('onSettled');
      if (data) {
        await queryClient.invalidateQueries('Messages');
        setErrMessage('');
        reset();
        clearErrors();
      }

      if (error) {
        setErrMessage(error.message);
      }
    },
    onSuccess: () => {
      console.log('onSuccess');
    },
    onError: async (error: any, _variables, context: any) => {
      setErrMessage(error.message);
      if (context?.previousMessages) {
        queryClient.setQueryData<MessageProps[]>("Messages", context.previousMessages);
      }
    }
  });

  const onSubmit = (data: MessageProps) => {
    mutation.mutate(data);
  };

  return (
    <Layout title="💌 Mama Muda" subTitle="Minta Pulsa">
      <Flex>
        <Box>
          <Box
            w="md"
            p={5}
            mr={4}
            border="1px"
            borderColor="gray.200"
            boxShadow="md"
          >
            <Text
              fontSize="xl"
              fontWeight="bold"
              mb={4}
              pb={2}
              borderBottom="1px"
              borderColor="gray.200"
            >
              ✍️ Request Pulsa
            </Text>
            <form>
              <FormControl pb={4} isInvalid={errors.phoneNumber ? true : false}>
                <FormLabel
                  htmlFor="phoneNumber"
                  fontWeight="bold"
                  fontSize="xs"
                  letterSpacing="1px"
                  textTransform="uppercase"
                >
                  Phone Number
                </FormLabel>
                <Input id="phoneNumber" name="phoneNumber" placeholder="Phone Number" ref={register({ required: 'Phone Number Required' })} />
                <FormErrorMessage>
                  {errors.phoneNumber && errors.phoneNumber.message}
                </FormErrorMessage>
              </FormControl>

              <FormControl isInvalid={errors.message ? true : false}>
                <FormLabel
                  htmlFor="name"
                  fontWeight="bold"
                  fontSize="xs"
                  letterSpacing="1px"
                  textTransform="uppercase"
                >
                  Message
                </FormLabel>
                <Textarea id="message" name="message" placeholder="Bullshit Message" ref={register({ required: 'Message Required' })} />
                <FormErrorMessage>
                  {errors.message && errors.message.message}
                </FormErrorMessage>
              </FormControl>

              <Button mt={4} colorScheme="teal" type="submit" onClick={handleSubmit(onSubmit)}>
                Send
              </Button>
            </form>
          </Box>
        </Box>
        <Box flex="1">
          { isSuccess && <MamaTable data={data} /> }
        </Box>
      </Flex>
    </Layout>
  );
}
