import { NextApiRequest, NextApiResponse } from 'next';

interface Message {
  id: string;
  createdAt?: string;
  phoneNumber: number;
  message: string;
  status?: string;
}
  
interface MessageStatus {
  id: string;
  status: string;
}

let messages: Message[] = [];

const addMessage = (body: Message): Message[] => {
  const preData = {
    id: messages.length + 1,
    createdAt: new Date().toISOString(),
    status: 'waiting'
  }

  const finalData = { ...preData, ...body };
  messages = [...messages, finalData];
  return [finalData];
}

const updateStatus = (body: MessageStatus) => {
  messages = messages.map((msg) => {
    if (msg.id == body.id) {
      msg.status = body.status;
    }
    return msg;
  });
  
  return [body];
}

export default function handler(req: NextApiRequest, res: NextApiResponse) {
    let response: Message[] | MessageStatus[] = []

    switch (req.method) {
        case 'GET':
            response = messages;
            break;

        case 'POST':
            response = addMessage(typeof req.body === 'string' ? JSON.parse(req.body) : req.body);
            break;

        case 'PUT':
            response = updateStatus(typeof req.body === 'string' ? JSON.parse(req.body) : req.body);
            break;
    }

    setTimeout(() => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(response));
    }, 1500);
}