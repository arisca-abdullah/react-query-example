import React, { useState } from "react";
import { useQuery, QueryClient } from 'react-query';
import { dehydrate } from 'react-query/hydration';
import {
  Badge,
  Flex,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  Image,
  Text,
  Spinner,
  Grid,
  Button
} from "@chakra-ui/react";
import Layout from "../../components/Layout";

type Price = {
  id: string,
  symbol: string,
  name: string,
  image: string,
  current_price: number,
  price_change_percentage_24h: number,
  total_volume: number,
  market_cap: number
}

const formatNumber = (num: number) => {
  return Intl.NumberFormat('id-Id').format(num);
}

const formatPercent = (num: number) => {
  return Intl.NumberFormat('id-Id', {
    style: 'percent',
    minimumFractionDigits: 1,
    maximumFractionDigits: 1
  }).format(num / 100);
}

const Percentage = ({ percent }: { percent: number }) => {
  return (
    <Text color={ percent < 0 ? 'red.500' : percent > 0 ? 'green.500' : 'black' }>
      { formatPercent(percent) }
    </Text>
  )
}

const getMarket = async (page = 1) => {
  const URL = `https://api.coingecko.com/api/v3/coins/markets?vs_currency=idr&per_page=10&page=${page}`;
  const response = await fetch(URL);
  
  if (!response.ok) {
    throw new Error('Failed to fetch data');
  }

  return await response.json();
}

export default function Market() {
  const [page, setPage] = useState(1);
  const { data, isError, isLoading, isFetching, isSuccess } = useQuery(
    ['Market', page],
    () => getMarket(page),
    {
      staleTime: 3000,
      refetchInterval: 3000
    }
  );
  return (
    <Layout title="Crypto Market">
      {isFetching && <Spinner color="blue.500" position="fixed" top={10} right={10} />}
      <Table variant="simple">
        <Thead>
          <Tr>
            <Th>Coin</Th>
            <Th>Last Price</Th>
            <Th>24h % Change</Th>
            <Th isNumeric>Total Volume</Th>
            <Th isNumeric>Market Cap</Th>
          </Tr>
        </Thead>
        <Tbody>
          {isLoading && <Text>Loading...</Text>}
          {isError && <Text color="red.500">There was an error processing your request.</Text>}
          {isSuccess && data?.map((price: Price) => (
            <Tr key={price.id}>
              <Td>
                <Flex alignItems="center">
                  <Image
                    src={price.image}
                    boxSize="24px"
                    ignoreFallback={true}
                  />
  
                  <Text pl={2} fontWeight="bold" textTransform="capitalize">
                    {price.name}
                  </Text>
                  <Badge ml={3}>{price.symbol}</Badge>
                </Flex>
              </Td>
              <Td>{ formatNumber(price.current_price) }</Td>
              <Td><Percentage percent={ price.price_change_percentage_24h } /></Td>
              <Td isNumeric>{ formatNumber(price.total_volume) }</Td>
              <Td isNumeric>{ formatNumber(price.market_cap) }</Td>
            </Tr>  
          ))}
        </Tbody>
      </Table>
      <Grid templateColumns="70% 1fr auto 1fr" gap={6} mt={10}>
        <div></div>
        <Button
          colorScheme="facebook"
          variant="outline"
          size="sm"
          onClick={() => setPage(old => old - 1)}
          disabled={page === 1}>
            Previous
        </Button>
        <Text>{page}</Text>
        <Button
          colorScheme="facebook"
          variant="outline"
          size="sm"
          onClick={() => setPage(old => old + 1)}>
          Next
        </Button>
      </Grid>
    </Layout>
  );
}

// SSR Initial Data
// export async function getStaticProps() {
//   const initialPrice = await getMarket();
//   return { props: { initialPrice } };
// }

// SSR with Hydration
export async function getStaticProps() {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(['Market', 1], () => getMarket());

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    }
  }
}